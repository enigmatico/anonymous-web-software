# bbs

This is a very simple and 100% anonymous bulletin board system that uses PHP. No database
is required.

To install it, copy the bbs folder into your web server. Then configure your
config.php and privileged.php files accordingly. Most importantly, change the
$salt variable in config.php with any long and secure password salt you want
to use.

# Requirements

- A web server
- PHP 7 or higher

# Browser compatibility

Literally every browser known to mankind will render this site.

# How the board works

The board is completely anonymous. Users with a Name will be identified with a
secure hash of their name.

To create a board, create a folder manually for that board inside the 'boards'
directory. Boards can have sub-boards too, which is subdirectories with no MAIN.md
file on them. Then, edit the boards.php, and add your board to the list.

Topics are generated inside each board/sub-board. Remember to place an empty index.html
inside each board/sub-board directory, for safety.

Topics are .md files inside their corresponding directory. Each .md file contains a single
post. These are markdown files (Yes, the board supports Markdown).

Once you have your BBS structure inside, you can create new topics inside each board by
accessing them and filling the 'Start a new topic' form.

Since the Name field is hashed, you can take it as a 'password' rather than a name to
identify yourself. Every time you put the same password here, you'll get the same hash,
and people will know that your posts are the same person.

Topic is just a topic name, and message is your message. Up to 4089 characters are
allowed (By default, can be changed in config.php).

# Trust system

Since this is an anonymous board, there is no such thing as "accounts" or "privileges"
in this board. However, there is a 'trust' system on which trusted users will have a
blue color on their hash identifier. This basically indicates that these users are
trusted by the board administrator.

To add users to the trusted zone, modify the privileged.php file, adding their hashes
to the $privileged array. For instance:

$privileged = [
	"398ijOcY4Rm1xUtTEpIe7lFCQ5yIXQTzluFAlockZQx2OFeDtwBwds7eqCCxItdZLBfk0r712Ga72kd02",
    "2eD02BvnGCSIKFxRi9RQcVCexLCdfInxaQsliB7XtEkPp0tKC8kZOm3YvBb7YLjWwOA0ZBLKisgnIpzFvK9"
];

The users with those hashes will have their hashes in blue.

The system administrator must place their hash in the $admin variable.
You can use the namegen.php utility to generate a secure admin hash you must set in the $admin
variable. For that, load this page with the url parameter 'name' and your nick. For instance:

https://yourhost/bbs/namegen.php?name=Yournamehere

This will print the hash for your username. You can even use curl for this if you
want.

# Moderation

There is no moderation board. Not that there is much need for it. Since each post is a markdown
file and each topic is a subdirectory on their respective boards, you can simply edit or delete
those files at your heart content.

# Change the MOTD

Just modify the motd.md file. Markdown is allowed.