# Enigmatico's Hidden Service Software

Those are a few web applications I wrote in HTML and PHP specifically for my hidden service.
Those services are designed to be lightweight and as simple as possible, to work in any
web browser (Even ancient ones), to work without any kind of database and to be completely
anonymous.

The following applications are available:

· **bbs**: A bulletin board system
· **gallery**: Image gallery
· **mdblog**: Blog

## Disclaimer

This software uses the following third party libraries:

· **htmlpurifier**: By Edward Z. Yang [LGPL license](https://github.com/ezyang/htmlpurifier/blob/master/LICENSE)
· **Parsedown**: By Emanuil Rusev [MIT license](https://github.com/erusev/parsedown/blob/master/LICENSE.txt)