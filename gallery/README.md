# gallery

This is a very simple image gallery that works with PHP and Imagemagick.

Installing and setting it up is trivial, its "plug and play". All you have to do is to
upload the gallery software into your web server. In order to create galleries, just
create folders with your images inside the 'galleries' folder. All image formats are
supported.

The software will generate thumbnails 'on the fly' for those images that don't have them
so it might take a little while to load for the first time. Once the thumbnails are
generated, this won't happen again.

# Navigation

Select the subfolder (or parent folder) you want to browse, then click GO. If you
want to switch pages, either use the page selector and click Go, or use the navigation
bar on top.

GO UP will take you to the parent folder. First, to the first page. Last, to the last
page. Next to the next page. Previous, to the previous page.

Click on any of the images to open it. You can also right click and select Download
to download the image.

# Requirements

- A web server
- PHP 7 or better.
- Imagemagick (remember to install php7-imagick)

# Browser compatibility

Literally every browser known to mankind will render this site.