# mdblog

This is a personal blog I used for my hidden service. It's designed to be extremely
lightweight, simple (doesn't even require a database), and to be compatible with every
browser in existance. This means that even ancient browsers can render this blog.

Installing it is really simple. Just copy the folder into your web server content
folder, change the $post_password variable to your password in composer.php, and
then open composer.php from your browser in order to redact your post. Don't forget
to specify your password there too.

Markdown is allowed in your posts.

Your posts will be stored in the content folder.

# Requirements

A web server and PHP version 7 or superior.

# Browser compatibility

Literally every browser known to mankind will render this site.